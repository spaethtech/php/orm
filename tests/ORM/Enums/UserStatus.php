<?php
declare(strict_types=1);

namespace SpaethTech\Tests\ORM\Enums;

enum UserStatus: int
{
	case NEW = 0; // Default
	case ACTIVE = 1;
	case SUSPENDED = 2;
}
