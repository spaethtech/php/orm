<?php
declare(strict_types=1);

namespace SpaethTech\Tests\ORM\Enums;

// TODO: Create a Flags-style Enum!!!

use SpaethTech\ORM\Traits\BitMaskEnum;

enum UserNotifications: int
{
    use BitMaskEnum;

    case NONE = 0; // Default
	case EMAIL = 1;
	case TEXT = 2;
	case DESKTOP = 4;

    case MOBILE = 8;


    //case ALL = 15;
}
