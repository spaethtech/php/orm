<?php
declare(strict_types=1);

namespace SpaethTech\Tests\ORM\Enums;

enum HostStatus : int
{
    case UNAUTHORIZED   = 0; // Default
    case AUTHORIZED     = 1;

    case WHITELISTED    = 2;
    case BLACKLISTED    = 3;

    case INTERNAL       = 9;

}
