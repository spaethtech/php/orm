<?php
declare(strict_types=1);

namespace SpaethTech\QueryBuilder;

use PDO;
use SpaethTech\ORM\Model;

/**
 * Statement
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 *
 * @template T of Model
 */
abstract class Statement
{
    protected array $parameters = [];

    /**
     * @param class-string<T> $model
     * @param PDO $pdo
     */
    public function __construct(protected string $model, protected PDO $pdo)
    {
    }

    /**
     * @return array<T>|NULL
     */
    public function fetchAll() : array|NULL
    {
        $query = $this->pdo->prepare((string)$this);

        if (!$query->execute($this->parameters))
            return NULL;

        $rows = $query->fetchAll(PDO::FETCH_ASSOC);
        $results = [];

        foreach($rows as $row)
            $results[] = new $this->model($row);

        return $results;
    }

    /**
     * @return Model|NULL
     */
    public function fetch()
    {
        $all = $this->fetchAll();
        return ($all === FALSE || count($all) === 0) ? NULL : $all[0];
    }

}
