<?php
declare(strict_types=1);

namespace SpaethTech\QueryBuilder\Clauses;

class WhereClause
{
    protected array $lines = [];

    protected function addLine(string $prefix, string $columnOrExpression,
        string $operator = null, string $namedParameter = null) : self
    {
        if ($operator === null && $namedParameter === null)
        {
            $this->lines[] = "$prefix $columnOrExpression";
        }
        else
        {
            if ($namedParameter === null || $namedParameter === "?")
                $namedParameter = "?";
            else
                if (!str_starts_with($namedParameter, ":"))
                    $namedParameter = ":$namedParameter";

            $this->lines[] = "$prefix $columnOrExpression $operator $namedParameter";
        }

        return $this;
    }

    public static function create(string $columnOrExpression, string $operator = null, string $namedParameter = null) : self
    {
        return new WhereClause($columnOrExpression, $operator, $namedParameter);
    }

    public function __construct(//protected Statement $parent,
        string $columnOrExpression, string $operator = null, string $namedParameter = null)
    {
        return $this->addLine("WHERE", $columnOrExpression, $operator, $namedParameter);
    }

//    public function statement() : Statement
//    {
//        return $this->parent;
//    }

    public function and(string $columnOrExpression, string $operator = null,
        string $namedParameter = null) : self
    {
        return $this->addLine("  AND", $columnOrExpression, $operator, $namedParameter);
    }

    public function or(string $columnOrExpression, string $operator = null,
        string $namedParameter = null) : self
    {
        return $this->addLine("   OR", $columnOrExpression, $operator, $namedParameter);
    }

    public function __toString() : string
    {
        return implode("\n", $this->lines);
    }

}
