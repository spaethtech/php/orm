<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\QueryBuilder;

use PDO;
use SpaethTech\ORM\Model;

/**
 * SelectStatement
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 *
 * @template T of Model
 * @template-extends Statement<T>
 */
class SelectStatement extends Statement
{
    protected string $table = "";

    /**
     * @param class-string<T> $model
     * @param PDO $pdo
     * @param string $table
     * @param string|NULL $alias
     */
    public function __construct(string $model, PDO $pdo, string $table, string $alias = null)
    {
        parent::__construct($model, $pdo);

        $this->table = $alias !== null && $alias !== ""
            ? "$table AS $alias"
            : $table;
    }

    #region Columns

    protected array $columns = [];

    /**
     * @param string $name
     * @param string|NULL $alias
     *
     * @return $this
     */
    public function withColumn(string $name, string $alias = NULL) : self
    {
        return $this->withColumns([ $name => $alias ]);
    }

    /**
     * @param array $columns
     *
     * @return $this
     */
    public function withColumns(array $columns) : self
    {
        foreach ($columns as $name => $alias)
            $this->columns[$name] = $alias !== null && $alias !== "" ? $alias : $name;

        return $this;
    }

    #endregion

    #region Grouping

    private int $groupingIndex = 0;

    public function group(string $operator = "AND") : self
    {
        $this->wheres[$this->groupingIndex] = [ "operator" => $operator ];
        return $this;
    }

    public function ungroup() : self
    {
        $this->groupingIndex++;
        return $this;
    }

    #endregion

    #region Filtering

    protected array $wheres = [];

    /**
     *
     * @param string $expression The expression with placeholders
     * @param mixed ...$parameters Any parameters to pass to the Statement
     *
     * @return $this
     */
    public function where(string $expression, &...$parameters) : self
    {
        if (!array_key_exists($this->groupingIndex, $this->wheres))
            $this->wheres[$this->groupingIndex] = [];

        if (!array_key_exists("operator", $this->wheres[$this->groupingIndex]))
            $this->wheres[$this->groupingIndex]["operator"] = "AND";

        if (!array_key_exists("members", $this->wheres[$this->groupingIndex]))
            $this->wheres[$this->groupingIndex]["members"] = [];

        $this->wheres[$this->groupingIndex]["members"][] = $expression;

        // TODO-FEATURE: Check parameter count vs placeholder count?

        foreach($parameters as &$parameter)
            $this->parameters[] = &$parameter;

        return $this;
    }

    #endregion

    /**
     * Override the __toString() method to output the actual SQL statement.
     *
     * @return string The SQL statement
     */
    public function __toString() : string
    {
        $lines = [];

        #region SELECT

        $columns = [];
        foreach ($this->columns as $name => $alias)
            $columns[] = $name === $alias ? $name : "$name AS $alias";

        if(count($columns) === 0)
            $columns[] = "*";

        $lines[] = "SELECT ".(implode(", ", $columns));

        #endregion

        #region FROM

        $lines[] = "  FROM $this->table";

        #endregion

        #region WHERE

        for($i = 0; $i < count($this->wheres); $i++)
        {
            $prefix = $i === 0
                ? " WHERE"
                : str_pad($this->wheres[$i]["operator"], 6, " ", STR_PAD_LEFT);

            $lines[] = "$prefix ( ".implode(" {$this->wheres[$i]["operator"]} ",
                $this->wheres[$i]["members"]) ." )";
        }

        #endregion

        // TODO-FUTURE: Decide if we ever need to use a JOIN or other syntax?

        return trim(implode("\n", $lines)).";";
    }

}
