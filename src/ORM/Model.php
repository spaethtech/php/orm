<?php
/** @noinspection DuplicatedCode, PhpUnused */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM;

use Attribute;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use JsonSerializable;
use Nette\PhpGenerator\PhpNamespace;
use PDO;
use PDOException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use SpaethTech\ORM\Attributes\Column;
use SpaethTech\ORM\Attributes\ExcludeIf;
use SpaethTech\ORM\Attributes\Generator;
use SpaethTech\ORM\Attributes\ID;
use SpaethTech\ORM\Attributes\Serializer;
use SpaethTech\ORM\Attributes\Table;
use SpaethTech\ORM\Attributes\Unique;
use SpaethTech\ORM\Attributes\Validate;
use SpaethTech\ORM\Exceptions\MethodNotFoundException;
use SpaethTech\ORM\Methods\DatabaseMethod;
use SpaethTech\ORM\Methods\HttpMethod;
use SpaethTech\ORM\Serializers\DateTimeSerializer;
use SpaethTech\ORM\Serializers\EnumSerializer;
use SpaethTech\ORM\Serializers\SerializerInterface;
use SpaethTech\QueryBuilder\SelectStatement;

/**
 * Model
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2022 - Spaeth Technologies Inc.
 *
 * @template T of Model
 */
abstract class Model implements JsonSerializable
{
    const DEFAULT_PROPERTY_FILTER = ReflectionProperty::IS_PUBLIC |
    ReflectionProperty::IS_PROTECTED;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach($data as $column => $value)
            $this->$column = $value;

        try
        {
            $className = get_called_class();
            $class = new ReflectionClass($className);
            $properties = $class->getProperties(self::DEFAULT_PROPERTY_FILTER);

            foreach($properties as $property)
            {
                if (!$property->isInitialized($this))
                {
                    $generators = $property->getAttributes(Generator::class);

                    // IF a Generator is present...
                    if (count($generators) === 1)
                    {
                        // ...THEN we need to use it!

                        /** @var Generator $generator */
                        $generator = $generators[0]->newInstance();

                        //$genFQCN = $generator->class;
                        //$genFunc = $generator->method;
                        //$genArgs = $generator->args;

                        $func = $generator->class !== null
                            ? "$generator->class::$generator->method"
                            : "$generator->method";

                        $value = call_user_func_array($func, $generator->args);

                        // Return the serialized value!
                        $property->setValue($this, $value);
                    }
                }
            }
        }
        catch(ReflectionException)
        {
        }
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    # region Converters

    /**
     * Converts the Model to an associative array where keys are the property or
     * column name and the values are the property values.
     *
     * @param bool $useColumnName When TRUE, use column NOT property names
     *
     * @return array
     */
    public function toArray(bool $useColumnName = FALSE) : array
    {
        $properties = [];

        foreach(self::$_cache[$this::class]["columns"] as $column => $info)
        {
            /** @var ReflectionProperty $property */
            $property = $info["property"];

            $properties[$useColumnName ? $column : $property->getName()] =
                $property->getValue($this);
        }

        return $properties;
    }

    /**
     * Converts the Model to JSON based on the output of {@see toArray()}.
     *
     * @param bool $useColumnName When TRUE, use column NOT property names
     * @param int $flags Refer to {@see json_encode()}
     * @param int $depth Refer to {@see json_encode()}
     *
     * @return string
     */
    public function toJSON(bool $useColumnName = FALSE, int $flags = 0,
        int $depth = 512) : string
    {
        return json_encode($this->toArray($useColumnName), $flags, $depth);
    }

    /**
     * @return string The Model represented as a JSON string.
     */
    public function __toString() : string
    {
        return $this->toJSON(FALSE, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    # endregion

    # region CRUD

    /**
     * Selects the Model's values from the DB.
     *
     * @param PDO $pdo The PDO to use for operations
     *
     * @return SelectStatement<T> For method chaining
     */
    public static function select(PDO $pdo) : SelectStatement
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        $table = self::$_cache[$class]["table"];
        return new SelectStatement($class, $pdo, $table);
    }

    /**
     * Inserts the Model's values into the DB.
     *
     * @param PDO $pdo
     * @param string|FALSE $id
     * @param array|null $errors
     *
     * @return static
     * @noinspection PhpDocSignatureInspection
     */
    public function insert(PDO $pdo, string|FALSE &$id = null, array &$errors = null) : static
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        $table = self::$_cache[$class]["table"];


        // Get ALL property names
        $properties = self::_getPropertyNames();

        # region Validate

        $errors = [];

        foreach(self::_findPropertiesWithAttribute(Validate::class)
            as $column => $property)
        {
            /** @var Validate $validate */
            $validate = $property->getAttributes(Validate::class)[0]
                ->newInstance();

            $pattern = $validate->regex;
            $value = (string)$this->$column;

            if (!preg_match($pattern, $value))
                $errors[] = "Property '$property' has invalid value '$value', ".
                    "based on RegExp: $pattern";
        }

        if (count($errors) > 0)
        {
            // TODO: Report error!

            return $this;
        }

        # endregion

        #region ExcludeIf

        foreach(self::_findPropertiesWithAttribute(ExcludeIf::class)
            as $column => $property)
        {
            foreach($property->getAttributes(ExcludeIf::class) as $exclude)
            {
                /** @var ExcludeIf $aExclude */
                $aExclude = $exclude->newInstance();
                if ($aExclude->method === DatabaseMethod::INSERT)
                {
                    if ($aExclude->eval === NULL || eval($aExclude->eval))
                    {
                        unset($properties[$column]);
                        break;
                    }
                }

            }


        }

        # endregion

        $cols = [];
        $keys = [];
        $vals = [];

        // Loop through each property...
        foreach($properties as $column => $propertyName)
        {
            $cols[] = $column;
            $keys[] = ":$column";
            $vals[$column] = $this->$column;
        }

        $cols = implode(", ", $cols);
        $keys = implode(", ", $keys);

//        dump($cols);
//        dump($keys);
//        dd($vals);

        //dump("INSERT INTO $table ( $cols ) VALUES ( $keys );");

        try
        {
            /** @noinspection SqlNoDataSourceInspection */
            $sql = <<<SQL
                INSERT INTO $table
                ( $cols )
                VALUES
                ( $keys );
            SQL;

//            switch($pdo->getAttribute(PDO::ATTR_DRIVER_NAME))
//            {
//                case "pgsql":
//                    $id = self::$_cache[$class]["id"];
//                    dump($id);
//                    $sql .= "\nRETURNING $id;";
//                    break;
//                default:
//                    $sql .= ";";
//            }

            $query = $pdo->prepare($sql);

            if (!$query->execute($vals))
            {
                // TODO: Report error!
                dump("FALSE");
                return $this;
            }

            if ($pdo->errorCode() !== PDO::ERR_NONE)
            {
                dump("ERROR");

            }

            $id = $pdo->lastInsertId();

            $idProperty = self::$_cache[$class]["id"];
            $this->$idProperty = $id;
        }
        catch(PDOException $pe)
        {

            dump("EXCEPTION", $pe);
        }


        return $this;
    }

    public function update(PDO $pdo, array &$errors = null) : static
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        $table = self::$_cache[$class]["table"];

        // Get ALL property names
        $properties = self::_getPropertyNames();

        # region Validate

        $errors = [];

        foreach(self::_findPropertiesWithAttribute(Validate::class)
            as $column => $property)
        {
            /** @var Validate $validate */
            $validate = $property->getAttributes(Validate::class)[0]
                ->newInstance();

            $pattern = $validate->regex;
            $value = (string)$this->$column;

            if (!preg_match($pattern, $value))
                $errors[] = "Property '$property' has invalid value '$value', ".
                    "based on RegExp: $pattern";
        }

        if (count($errors) > 0)
        {
            // TODO: Report error!

            return $this;
        }

        # endregion

        #region ExcludeIf

        foreach(self::_findPropertiesWithAttribute(ExcludeIf::class)
            as $column => $property)
        {
            foreach($property->getAttributes(ExcludeIf::class) as $exclude)
            {
                /** @var ExcludeIf $aExclude */
                $aExclude = $exclude->newInstance();
                if ($aExclude->method === DatabaseMethod::UPDATE)
                {
                    if ($aExclude->eval === NULL || eval($aExclude->eval))
                    {
                        unset($properties[$column]);
                        break;
                    }
                }

            }


        }

        # endregion

        $cols = [];
        $keys = [];
        $sets = [];
        $vals = [];

        // Loop through each property...
        foreach($properties as $column => $propertyName)
        {
            $cols[] = $column;
            $keys[] = ":$column";
            $sets[] = "$column = :$column";
            $vals[$column] = $this->$column;
        }

        $cols = implode(", ", $cols);
        $keys = implode(", ", $keys);
        $sets = implode(", ", $sets);

        //dump($cols);
        //dump($keys);
        //dump($sets);
        //dd($vals);

        //dump("INSERT INTO $table ( $cols ) VALUES ( $keys );");

        try
        {
            $idProperty = self::$_cache[$class]["id"];
            $id = $this->$idProperty;

            $vals["_id"] = $id;

            /** @noinspection SqlNoDataSourceInspection */
            $sql = <<<SQL
                UPDATE $table
                SET $sets
                WHERE $idProperty = :_id;
            SQL;

            $query = $pdo->prepare($sql);

            if (!$query->execute($vals))
            {
                // TODO: Report error!
                dump("FALSE");
                return $this;
            }

            if ($pdo->errorCode() !== PDO::ERR_NONE)
            {
                dump("ERROR");

            }

            //$id = $pdo->lastInsertId();
        }
        catch(PDOException $pe)
        {

            dump("EXCEPTION", $pe);
        }


        return $this;
    }



    #endregion

    #region Caching

    private static array $_cache = [];

    /**
     * @param class-string<T> $className
     *
     * @return void
     */
    private static function _buildCacheFromClass(string $className) : void
    {
        try
        {
            $class = new ReflectionClass($className);

            // Cache!
            if (!array_key_exists($class->name, self::$_cache))
            {
                $props = $class->getProperties(
                    ReflectionProperty::IS_PUBLIC |
                    ReflectionProperty::IS_PROTECTED |
                    ReflectionProperty::IS_PRIVATE
                );

                $aTable = $class->getAttributes(Table::class);

                self::$_cache[$class->name] = [
                    "table" => count($aTable) > 0
                        ? $aTable[0]->newInstance()->name
                        : strtolower(basename($class->name)),
                    "columns" => [],
                ];

                foreach ($props as $prop)
                {
                    $aColumn = $prop->getAttributes(Column::class);
                    if(count($aColumn) !== 1)
                        continue;

                    $columnName = $aColumn[0]->newInstance()->name;

                    $aID = $prop->getAttributes(ID::class);

                    if(count($aID) > 0)
                    {
                        $iKey = $aID[0]->newInstance();
                        //if($iKey->primary)
                        self::$_cache[$class->name]["id"] = $columnName;
                    }

                    self::$_cache[$class->name]["columns"][$columnName] = [
                        "id" => count($aID) > 0,
                        "property" => new ReflectionProperty($className, $prop->name),
                    ];

                    // Other caching here?
                }
            }
        }
        catch (ReflectionException)
        {
            // TODO: How to handle?
        }
    }

    private function _findColumn(string $propertyName) : string|false
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        foreach(self::$_cache[$class]["columns"] as $column => $property)
            if ($property["property"]->getName() === $propertyName)
                return $column;

        return FALSE;
    }

    /**
     * @param class-string<Attribute> $attributeName
     *
     * @return array<ReflectionProperty>
     */
    private function _findPropertiesWithAttribute(string $attributeName) : array
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        $properties = [];

        foreach(self::$_cache[$class]["columns"] as $column => $record)
        {
            /** @var ReflectionProperty $property */
            $property = $record["property"];

            if (count($attributes = $property->getAttributes($attributeName)) > 0)
                $properties[$column] = $property;
        }

        return $properties;
    }

    private function _getPropertyNames() : array
    {
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        return array_map(fn($i) => $i["property"]->getName(),
            self::$_cache[$class]["columns"]);
    }




    #endregion





    #region Magic Methods

    /**
     * Handles setting the value of a dynamic/virtual property.
     *
     * @param string $name The name of the virtual property (column)
     * @param mixed $value The value to set
     *
     * @return void
     */
    public function __set(string $name, mixed $value) : void
    {
        // Build the cache as needed
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        // Get the list of Column mappings
        $columns = self::$_cache[$this::class]["columns"];

        // IF no Column mapping exists, ignore this property!
        if (!array_key_exists($name, $columns))
            return;

        /** @var ReflectionProperty $property */
        $property = $columns[$name]["property"];

        // Get any associated Serializers
        $serializers = $property->getAttributes(Serializer::class);

        // IF a Serializer is present...
        if(count($serializers) === 1)
        {
            // ...THEN we need to use it!
            $serializerClass = $serializers[0]->newInstance()->serializer;

            /** @var SerializerInterface $serializer */
            $serializer = new $serializerClass($property, $name);

//            if($property->getName() === "allowed")
//            {
//                dump($value);
//            }

            $property->setValue($this, $serializer->deserialize($value));
        }
        else
        {
            // ...OTHERWISE, set the actual value!
            $property->setValue($this, $value);
        }

    }

    /**
     * Handles getting the value of a dynamic/virtual property.
     *
     * @param string $name The name of the virtual property (column)
     *
     * @return mixed The value from the property or associated Serializer
     */
    public function __get(string $name) : mixed
    {
        // Build the cache as needed
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        // Get the list of Column mappings
        $columns = self::$_cache[$this::class]["columns"];

        // IF no Column mapping exists, ignore this property!
        if (!array_key_exists($name, $columns))
            return null;

        /** @var ReflectionProperty $property */
        $property = $columns[$name]["property"];

        $value = $property->isInitialized($this)
            ? $property->getValue($this)
            : ($property->getType()->allowsNull()
                ? NULL
                : $property->getDefaultValue());

        #region Serialization

        // Get any associated Serializers
        $serializers = $property->getAttributes(Serializer::class);

        // IF a Serializer is present...
        if(count($serializers) === 1)
        {
            // ...THEN we need to use it!
            $serializerClass = $serializers[0]->newInstance()->serializer;

            /** @var SerializerInterface $serializer */
            $serializer = new $serializerClass($property, $name);

            // Return the serialized value!
            return $serializer->serialize($value);
        }

        # endregion

        // Return the actual value!
        return $value;
    }

    /**
     * Handles dynamic/virtual methods.
     *
     * @param string $name The name of the method being called.
     * @param array $arguments The arguments being passed.
     *
     * @return mixed|static The value from getX() or THIS from setX()
     *
     * @throws MethodNotFoundException
     *
     * @noinspection PhpMixedReturnTypeCanBeReducedInspection
     */
    public function __call(string $name, array $arguments) : mixed
    {
        // Build the cache as needed
        $class = get_called_class();
        self::_buildCacheFromClass($class);

        $column = $this->_findColumn(substr($name, 3));
        //dump($column);

        /** @noinspection PhpUnhandledExceptionInspection */
        $property = new ReflectionProperty($class, substr($name, 3));

        // getX()
        if (str_starts_with($name, "get"))
        {
            return $property->isInitialized($this)
                ? $property->getValue($this)
                : ($property->getType()->allowsNull()
                    ? NULL
                    : $property->getDefaultValue());

            //return $this->{substr($name, 3)};
        }

        // setX()
        if (str_starts_with($name, "set"))
        {
            $property->setValue($this, $arguments[0]);
            //$this->{substr($name, 3)} = $arguments[0];

            // Return THIS to allow for method chaining
            return $this;
        }

        // A different dynamic method has been called!
        throw new MethodNotFoundException("No method exists named $name");
    }

    #endregion


//    private static function camelToSnake(string $input) : string
//    {
//        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
//    }
//    private static function snakeToCamel(string $input) : string
//    {
//        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $input))));
//    }

    private static function snakeToPascal(string $input) : string
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
    }


    /**
     * Builds the Model class for a given table, based on schema information
     * from a PostgreSQL database.
     *
     * @param PDO $pdo The PDO to use for schema collection
     * @param string $tableName The name of the table in the database
     * @param string $className The name of the desired class
     * @param string $rootNamespace The root namespace, defaults to `App`
     * @param string $rootDir The root output directory, defaults to `src`
     *
     * @return string
     */
    public static function build(PDO $pdo, string $tableName, string $className,
        string $rootNamespace = "App", string $rootDir = "src") : string
    {
        # region Table Keys

        /** @noinspection SqlNoDataSourceInspection */
        $rows = $pdo->query(<<<SQL
            SELECT a.attname AS column_name,
                   format_type(a.atttypid, a.atttypmod) AS column_type,
                   i.indisprimary AS is_primary,
                   i.indisunique AS is_unique
            FROM   pg_index AS i
            JOIN   pg_attribute AS a
                   ON  a.attrelid = i.indrelid
                   AND a.attnum = ANY(i.indkey)
            WHERE  i.indrelid = '$tableName'::regclass;
            SQL
        )->fetchAll(PDO::FETCH_ASSOC);

        $keys = [];
        foreach($rows as $row)
        {
            $keys[$row["column_name"]] = [
                "column_type" => $row["column_type"],
                "is_primary" => $row["is_primary"],
                "is_unique" => $row["is_unique"],
            ];
        }

        # endregion

        $modelsNamespace = "$rootNamespace\\Models";
        $enumsNamespace = "$rootNamespace\\Enums";

        if (str_contains($className, "\\"))
        {
            $modelsNamespace .= ("\\".dirname($className));
            $className = basename($className);
        }

        $namespace = new PhpNamespace($modelsNamespace);
        $namespace->addUse(Model::class);
        $namespace->addUse(Table::class);
        $namespace->addUse(Column::class);

        $class = $namespace->addClass($className);
        $class->setExtends(Model::class);
        $class->addAttribute(Table::class, [ $tableName ]);

        $getters = [];
        $setters = [];

        /** @noinspection SqlNoDataSourceInspection */
        $rows = $pdo->query(<<<SQL
            SELECT *,
                col_description((table_schema||'.'||table_name)::regclass::oid,
                ordinal_position) AS column_comment
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = '$tableName'
            ORDER BY ordinal_position;
            SQL
        )->fetchAll(PDO::FETCH_ASSOC);

        foreach($rows as $row)
        {
            $schema     = $row["table_schema"]; // public
            $column     = $row["column_name"]; // u_on
            $position   = $row["ordinal_position"]; // 32
            $default    = $row["column_default"]; // 32
            $nullable   = $row["is_nullable"]; // NO
            $dataType   = $row["data_type"]; // timestamp
            $columnType = $row["udt_name"]; // timestamp
            $columnKey  = $row["is_identity"];
            //$extra      = $row["extra"]; // on update current_timestamp()
            $generated  = $row["is_generated"]; // NEVER
            $increment  = FALSE;
            $comment    = $row["column_comment"];

            # region Sequence

            $reAutoIncrement = "|^nextval\('{$tableName}_id_seq'::regclass\)$|";
            if($default !== NULL && preg_match($reAutoIncrement, $default, $matches))
                $increment = TRUE;

            # endregion

            #region Property Name

            // Common Property Name modifications...
            /** @noinspection PhpMatchExpressionWithOnlyDefaultArmInspection */
            $propertyName = match($column)
            {
//                "c_on"          => "createdOn",
//                "c_by"          => "createdBy",
//                "c_from"        => "createdFrom",
//                "u_on"          => "updatedOn",
//                "u_by"          => "updatedBy",
//                "u_from"        => "updatedFrom",
//                "l_first_name"  => "firstName",
//                "l_last_name"   => "lastName",
                default         => self::snakeToPascal($column),
            };

//            if (str_ends_with($propertyName, "Sid"))
//                $propertyName = substr($propertyName, 0, -3)."SID";
//
//            if (str_ends_with($propertyName, "Id"))
//                $propertyName = substr($propertyName, 0, -2)."ID";

            $property = $class->addProperty($propertyName);
            $property->setProtected();

            #endregion

            # region Column

            if (array_key_exists($column, $keys))
            {
                if ($keys[$column]["is_primary"])
                {
                    $namespace->addUse(ID::class);
                    $property->addAttribute(ID::class);
                }

                if (!$keys[$column]["is_primary"] &&
                    $keys[$column]["is_unique"])
                {
                    $namespace->addUse(Unique::class);
                    $property->addAttribute(Unique::class);

                    $namespace->addUse(ExcludeIf::class);
                    $namespace->addUse(DatabaseMethod::class);
                    $property->addAttribute(ExcludeIf::class, [
                        DatabaseMethod::UPDATE,
                    ]);
                }

                // TODO: Check other key types?
            }

            $property->addAttribute(Column::class, [ $column ]);

            #endregion

            #region Type

            $property->setNullable($nullable === "YES");

            /** @noinspection PhpDuplicateMatchArmBodyInspection */
            /** @noinspection SpellCheckingInspection */
            $type = match($dataType) {
                "bigint", "bigserial", "int8" => "int",
                "bit", "bit varying", "varbit" => "string",
                "boolean", "bool" => "bool",
                "box" => NULL,
                "bytea" => "array",
                "character", "char", "character varying", "varchar" => "string",
                "cidr" => NULL,
                "circle" => NULL,
                "date" => DateTime::class,
                "double precision", "float8" => "float",
                "inet" => NULL,
                "integer", "int", "int4" => "int",
                "interval" => DateInterval::class,
                "json" => "string",
                "jsonb" => NULL,
                "line", "lseg" => NULL,
                "macaddr", "macaddr8" => NULL,
                "money" => "float",
                "numeric", "decimal" => "float",
                "path" => NULL,
                "pg_lsn", "pg_snapshot" => NULL,
                "point", "polygon" => NULL,
                "real", "float4" => "float",
                "smallint", "int2", "smallserial", "serial2", "serial", "serial4" => "int",
                "text" => "string",
                "time without time zone", "time with time zone", "timetz" => DateTime::class,
                "timestamp without time zone", "timestamp with time zone", "timestamptz" => DateTime::class,
                "tsquery", "tsvector", "txid_snapshot" => NULL,
                "uuid" => "string",
                "xml" => "string",
                "ARRAY" => "array",
                "USER-DEFINED" => "enum",
                default => NULL
                //default => throw new Exception("Unexpected type: $dataType")
            };

            if ($type === NULL)
                dd("Unsupported Type: $type");

            if ($type === "array")
            {
                dd("Arrays are not currently supported!");
            }

            if ($comment !== NULL)
            {
                $reEnum = "|^Enum:(.*)$|";
                if (preg_match($reEnum, $comment, $matches))
                {
                    $namespace->addUse($type = $matches[1]);
                    $namespace->addUse(Serializer::class);
                    $namespace->addUse(EnumSerializer::class);
                    $property->addAttribute(Serializer::class, [ EnumSerializer::class ]);
                }

                $reArray = "|^Array:(.*)$|";
                if (preg_match($reArray, $comment, $matches))
                {
                    // TODO: Add array support as needed?
                    dd("Arrays are not currently supported!");
                }
            }

//            if ($type === "enum")
//            {
//                dd("enum");
//                $type = $enumsNamespace."\\".self::snakeToPascal($columnType);
//
//                if (!enum_exists($type))
//                {
//                    $rows = $pdo->query("SELECT enum_range(NULL::$columnType)")
//                        ->fetchAll(PDO::FETCH_ASSOC);
//
//                    if (count($rows) === 1)
//                    {
//                        $enumName = self::snakeToPascal($columnType);
//
//                        $enumCode = new PhpNamespace($enumsNamespace);
//                        $enum = $enumCode->addEnum(self::snakeToPascal($columnType));
//                        $enum->setType("string");
//
//                        $values = $rows[0]["enum_range"];
//                        $values = explode(",", ltrim(rtrim($values, "}"), "{"));
//
//                        foreach($values as $value)
//                            $enum->addCase(strtoupper($value), $value);
//
//                        $declare = "declare(strict_types=1);";
//                        $enumPhp = <<<PHP
//                            <?php
//                            $declare
//                            $enumCode
//                            PHP;
//
//                        $dir = "$rootDir/".str_replace("\\", "/", $enumsNamespace);
//
//                        if (!file_exists($dir))
//                            mkdir($dir, 0777, TRUE);
//
//                        file_put_contents("$dir/$enumName.php", $enumPhp);
//
//                    }
//
//                }
//
//                $namespace->addUse($type);
//                $namespace->addUse(Serializer::class);
//                $namespace->addUse(EnumSerializer::class);
//                $property->addAttribute(Serializer::class, [ EnumSerializer::class ]);
//            }

            if ($type === DateTime::class)
            {
                if (in_array($column, [ "created", "updated" ]))
                {
                    $namespace->addUse(ExcludeIf::class);
                    $namespace->addUse(DatabaseMethod::class);
                    $property->addAttribute(ExcludeIf::class, [
                        DatabaseMethod::INSERT,
                    ]);
                    $property->addAttribute(ExcludeIf::class, [
                        DatabaseMethod::UPDATE,
                    ]);
                }

                $namespace->addUse(DateTime::class);
                $namespace->addUse(Serializer::class);
                $namespace->addUse(DateTimeSerializer::class);
                $property->addAttribute(Serializer::class, [
                    DateTimeSerializer::class,
                ]);
            }

            if ($type === DateInterval::class)
            {
                $namespace->addUse(DateInterval::class);
                // TODO: DateInterval Serializer?
            }

            if($increment)
            {
                $namespace->addUse(ExcludeIf::class);
                $namespace->addUse(DatabaseMethod::class);
                $property->addAttribute(ExcludeIf::class, [
                    DatabaseMethod::INSERT,
                ]);
                $property->addAttribute(ExcludeIf::class, [
                    DatabaseMethod::UPDATE,
                ]);
            }

            $property->setType($type);

            #endregion

            #region Default

//            $defaultValue = ($default === "NULL" && $nullable === "YES")
//                ? null : match($type) {
//                    //"bool" => $default === "1",
//                    //"int" => intval($default),
//                    //"float" => floatval($default),
//                    //"string" => trim($default ?? "", "'\""),
//                    //"array" => $default,
//                    default => ""
//                };
//
//            if ($default === "now()")
//                $defaultValue = "";
//
//            if ($type === "array")
//            {
//                $parts = explode("::", $defaultValue);
//                $values = explode(",", ltrim(rtrim($parts[0], "}'"), "'{"));
//                $defaultValue = array_filter(array_map("trim", $values));
//            }
//
//            if ($increment)
//            {
//                $defaultValue = "";
//            }
//
//            if ($defaultValue !== "")
//                $property->setValue($defaultValue);

            #endregion

            #region Magic Methods

            if (str_contains($type, "\\"))
            {
                $namespace->addUse($type);
                $type = basename($type);
            }

            $strType = $type.($nullable === "YES" ? "|null" : "");
            $strMethod = ucfirst($propertyName);

            // Getter
            $getters[] = "@method $strType get$strMethod()";

            // Setter, only if the field is NOT incremented!
            if (!$increment)
                $setters[] = "@method self set$strMethod($strType \$$propertyName)";

            #endregion
        }

        $class->addComment("Class $className");
        $class->addComment("");
        $class->addComment("@author Ryan Spaeth <rspaeth@spaethtech.com>");
        $class->addComment("@copyright 2024 Spaeth Technologies Inc.");
        $class->addComment("");

        foreach($getters as $getter)
            $class->addComment("$getter");

        $class->addComment("");

        foreach($setters as $setter)
            $class->addComment("$setter");

        try
        {
            $timestamp = new DateTime("now", new DateTimeZone("America/Chicago"));
            $timestamp = $timestamp->format("Y-m-d H:i:s");
        }
        catch (Exception $e)
        {
            $timestamp = "";
        }

        $class->addComment("");
        $class->addComment("@generated by Robo $timestamp");
        $class->addComment("");
        $class->addComment("@template-extends Model<self>");

        $declare = "declare(strict_types=1);";
        $php = <<<PHP
        <?php
        /** @noinspection PhpUnused */
        /** @noinspection PhpMultipleClassDeclarationsInspection */
        $declare

        $namespace

        PHP;

        // Fix-Up Attributes, as they don't seem to currently "namespace" with Nette!
        $php = str_replace("'SpaethTech\ORM\Serializers\EnumSerializer'", "EnumSerializer::class", $php);
        $php = str_replace("'SpaethTech\ORM\Serializers\DateTimeSerializer'", "DateTimeSerializer::class", $php);

        // Fix-Up Enums, as they don't seem to currently "namespace" with Nette!
        $php = str_replace("\\".DatabaseMethod::class, "DatabaseMethod", $php);
        $php = str_replace("\\".HttpMethod::class, "HttpMethod", $php);

        $dir = "$rootDir/".str_replace("\\", "/", $modelsNamespace);

        if (!file_exists($dir))
            mkdir($dir, 0777, TRUE);

        file_put_contents("$dir/$className.php", $php);

        return $php;
    }


}

