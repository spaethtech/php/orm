<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

use BackedEnum;

/**
 * Class EnumSerializer
 *
 * Handles (de-)serialization of {@see BackedEnum} to SQL `string|int` values.
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2024 Spaeth Technologies Inc.
 */
class EnumSerializer extends AbstractSerializer
{

    public function serialize(mixed $property) : string|int
    {
        return $property->value;
    }

    public function deserialize(mixed $column) : BackedEnum
    {
        /** @var BackedEnum $type */
        $type = $this->property->getType()->getName();
        //dump($column);
        //dump($type);
        //dump($type::from($column));
        return $type::from($column);
        //return $column;
    }

}
