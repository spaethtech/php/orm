<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

class MySqlBoolSerializer extends AbstractSerializer
{
    public function serialize(mixed $property) : int
    {
        // True and False literals do not seem to work directly with MySql!
        return $property ? 1 : 0;
    }

    public function deserialize(mixed $column) : bool
    {
        return $column === 1;
    }

}
