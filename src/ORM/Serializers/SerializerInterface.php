<?php
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

interface SerializerInterface
{
    /**
     * Serializes the Proxy property to its real value.
     *
     * @param mixed $property The value to serialize
     *
     * @return mixed The real value
     */
    public function serialize(mixed $property) : mixed;

    /**
     * Deserializes the real value to its Proxy property.
     *
     * @param mixed $column The value to deserialize
     *
     * @return mixed The Proxy value
     */
    public function deserialize(mixed $column) : mixed;
}
