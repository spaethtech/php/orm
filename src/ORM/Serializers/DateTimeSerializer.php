<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

use DateTime;

/**
 * Class DateTimeSerializer
 *
 * Handles (de-)serialization of {@see DateTime} to SQL `timestamp` values.
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2024 Spaeth Technologies Inc.
 */
class DateTimeSerializer extends AbstractSerializer
{
    public function serialize(mixed $property) : ?string
    {
        // NOTE: Might as well pass the microseconds, as at least Postgres
        // expects it!
        return $property?->format("Y-m-d H:i:s.u");
    }

    public function deserialize(mixed $column) : ?DateTime
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $column === NULL ? NULL : new DateTime($column);
    }

}
