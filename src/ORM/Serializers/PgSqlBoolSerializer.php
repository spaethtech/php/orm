<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

/**
 * Class BoolSerializer
 *
 * Handles (de-)serialization of `bool` to SQL `boolean` values.
 *
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @copyright 2024 Spaeth Technologies Inc.
 */
class PgSqlBoolSerializer extends AbstractSerializer
{
    public function serialize(mixed $property) : string
    {
        // True and False literals do not seem to work directly with PgSql!
        return $property ? "true" : "false";
    }

    public function deserialize(mixed $column) : bool
    {
        return $column;
    }

}
