<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Serializers;

use ReflectionProperty;

abstract class AbstractSerializer implements SerializerInterface
{
    public function __construct(protected ReflectionProperty $property,
        protected string $columnName, protected array $args = [])
    {
    }
}
