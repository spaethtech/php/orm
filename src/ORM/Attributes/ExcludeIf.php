<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Attributes;

use Attribute;
use SpaethTech\ORM\Methods\DatabaseMethod;
use SpaethTech\ORM\Methods\HttpMethod;

#[Attribute(Attribute::TARGET_PROPERTY | Attribute::IS_REPEATABLE)]
final class ExcludeIf
{
    public function __construct(public DatabaseMethod|HttpMethod $method,
        public ?string $eval = NULL)
    {
    }
}
