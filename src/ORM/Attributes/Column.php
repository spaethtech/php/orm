<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
final class Column
{
    public function __construct(public string $name)
    {
    }
}
