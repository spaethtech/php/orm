<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Attributes;

use Attribute;
use SpaethTech\ORM\Serializers\SerializerInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
final class Serializer
{
    /**
     * @param class-string<SerializerInterface> $serializer
     */
    public function __construct(public string $serializer, public array $args = [])
    {
    }
}
