<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Attributes;

use Attribute;
use JetBrains\PhpStorm\Language;

#[Attribute(Attribute::TARGET_PROPERTY)]
final class Validate
{
    public function __construct(#[Language("RegExp")] public string $regex)
    {
    }
}
