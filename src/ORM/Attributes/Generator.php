<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Attributes;

use;
use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
final class Generator
{
    /**
     * @param class-string|NULL $class An optional fully-qualified class name
     * @param string $method The method or function to use
     * @param array $args Any optional arguments to pass
     */
    public function __construct(public ?string $class, public string $method,
        public array $args = [])
    {
    }
}
