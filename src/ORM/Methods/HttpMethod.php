<?php

namespace SpaethTech\ORM\Methods;

enum HttpMethod : string
{
    case POST   = "POST";   // C
    case GET    = "GET";    // R
    case PATCH  = "PATCH";  // U
    case DELETE = "DELETE"; // D

}
