<?php

namespace SpaethTech\ORM\Methods;

enum DatabaseMethod : string
{
    case INSERT = "INSERT"; // C
    case SELECT = "SELECT"; // R
    case UPDATE = "UPDATE"; // U
    case DELETE = "DELETE"; // D

}
