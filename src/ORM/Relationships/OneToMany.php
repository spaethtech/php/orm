<?php /** @noinspection PhpUnused, PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace SpaethTech\ORM\Relationships;

use Attribute;
use SpaethTech\ORM\Model;

#[Attribute(Attribute::TARGET_PROPERTY)]
readonly final class OneToMany
{
    /**
     * @param class-string<Model> $other
     */
    public function __construct(public string $other)
    {
    }
}
