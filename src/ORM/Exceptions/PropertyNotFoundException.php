<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\ORM\Exceptions;

use Exception;

class PropertyNotFoundException extends Exception
{
}
