<?php
/** @noinspection PhpDocSignatureIsNotCompleteInspection */
declare(strict_types=1);

use Robo\Symfony\ConsoleIO;
use Robo\Tasks;
use SpaethTech\ORM\Model;

require_once __DIR__."/vendor/autoload.php";

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends Tasks
{
    private PDO $pdo;

    private function getPDO() : PDO
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();

        $host = $_ENV["DB_HOST"];
        $name = $_ENV["DB_NAME"];
        $user = $_ENV["DB_USER"];
        $pass = $_ENV["DB_PASS"];

        return new PDO("pgsql:dbname=$name;host=$host;port=5432", $user, $pass,
            [
                // Default mode set to Object, because you like it :-)
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]);
    }

//    /**
//     * @command pdo:model:create-all
//     *
//     * @return void
//     */
//    public function pdoModelCreateAll(ConsoleIO $io) : void
//    {
//        $map = [
//            "attachments" => "Attachment",
//            "blacklist" => "Blacklist",
//            "contacts" => "Contact",
//            "crmapp_splynx" => "SplynxApp",
//            "crmapp_uisp" => "UispApp",
//            "groups" => "Group",
//            "log_api" => "LogApi",
//            "log_audit" => "LogAudit",
//            "log_logins" => "LogLogins",
//            "preregisters" => "PreRegisters",
//            "provider_twilio" => "TwilioProvider",
//            "settings" => "Settings",
//            "tags" => "Tag",
//            "textapp_simplereplys" => "TextApp\\SimpleReply",
//            "textapp_smartreplys" => "TextApp\\SmartReply",
//            "textapp_smartrules" => "TextApp\\SmartRule",
//            "textapp_texts" => "TextApp\\Text",
//            "users" => "User",
//            "users_meta" => "UserMeta",
//            "whitelist" => "Whitelist",
//            "workspace_providers" => "WorkspaceProvider",
//            "workspaces" => "Workspace",
//        ];
//
//        foreach ($map as $table => $class)
//        {
//            $this->pdoModelCreate($io, $table, $class);
//        }
//
//    }

    /**
     * @command pdo:model:create
     *
     * @param string $tableName
     * @param string $className
     *
     * @return void
     * @throws Exception
     */
    public function pdoModelCreate(ConsoleIO $io, string $tableName, string $className) : void
    {
        $php = Model::build($this->getPDO(), $tableName, $className,
            "Ethlos\\ORM", __DIR__."/tests");

        //print_r($php);
    }



}

